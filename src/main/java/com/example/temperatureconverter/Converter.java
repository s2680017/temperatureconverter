package com.example.temperatureconverter;

public class Converter {
    public double getTemperatureInF(String celsius) {
        return Double.parseDouble(celsius)*1.8+32;
    }
}

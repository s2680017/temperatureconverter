package com.example.temperatureconverter;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class TempConvert extends HttpServlet {
    private  Converter converter;

    public void init() {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in C: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Temperature in F: " +
                Double.toString(converter.getTemperatureInF(request.getParameter("celsius"))) +
                "</BODY></HTML>");
    }


    public void destroy() {
    }
}
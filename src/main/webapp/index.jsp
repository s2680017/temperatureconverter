<!DOCTYPE HTML>
<!-- Front end for the webBookQuote Servlet. -->

<html lang="en">
<head>
    <title>Temperature converter</title>
    <link rel="stylesheet" href="styles.css">
    <meta charset="UTF-8">
</head>

<body>
<h1>Web Book Quote Application</h1>

<form ACTION="./tempConvert">
    <p>Enter a temperature in C:  <input TYPE="TEXT" NAME="celsius"></p>
    <input TYPE="SUBMIT">
</form>

</body>
</html>